var rewroteVoteButton = false;
var rewroteCompleteButton = false;
var intervalId = setInterval(tryRewriteButton, 30);        

function tryRewriteButton()
{
    var voteButton = document.getElementById("pull-request-vote-button");
    if (voteButton != null && !rewroteVoteButton)
    {
        var newDiv = document.createElement("div");
        var bold = document.createElement("b");
        bold.appendChild(document.createTextNode("Like"));
        newDiv.appendChild(bold);
        voteButton.removeChild(voteButton.childNodes[0]);
        voteButton.appendChild(newDiv);
        rewroteVoteButton = true;
    }

    var completeButton = document.getElementById("pull-request-complete-button");
    if (completeButton != null && !rewroteCompleteButton)
    {
        var newDiv = document.createElement("div");
        var bold = document.createElement("b");
        bold.appendChild(document.createTextNode("Subscribe"));
        newDiv.appendChild(bold);
        completeButton.removeChild(completeButton.childNodes[0]);
        completeButton.appendChild(newDiv);
        rewroteCompleteButton = true;
    }

    console.log("In interval");

    if (rewroteVoteButton && rewroteCompleteButton)
    {
        clearInterval(intervalId);
        console.log("Interval cleared");
    }
}